package com.ericssonlabs;

import org.xboot.test.DeviceIO;
import com.google.zxing.WriterException;
import com.zxing.activity.CaptureActivity;
import com.zxing.encoding.EncodingHandler;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class BarCodeTestActivity extends Activity {
	private ImageView qrImgImageView;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		qrImgImageView = (ImageView) this.findViewById(R.id.iv_qr_image);
		makeZxingCode();
	}

	private void makeZxingCode() {
		try {
			String contentString = DeviceIO.read("/sys/bus/i2c/drivers/xic/6-0020/uniqueid").replaceAll("\r|\n", "");
			if (!contentString.equals("")) {

				Bitmap qrCodeBitmap = EncodingHandler.createQRCode(
						contentString, 350);
				qrImgImageView.setImageBitmap(qrCodeBitmap);
			} else {
				Toast.makeText(BarCodeTestActivity.this,
						"Text can not be empty", Toast.LENGTH_SHORT).show();
			}

		} catch (WriterException e) {
			e.printStackTrace();
		}
	}
}
